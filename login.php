<?php 

  if(isset($_POST["email"]) && isset($_POST["password"])){
    include './includes/conn.php';

    $email = $_POST["email"];
    $password = $_POST["password"];
    $passCript = md5($password);
  
    $select = $conn->query("SELECT * FROM `usuarios` WHERE email='$email' AND senha='$passCript'");
    $results = $select->fetchAll(PDO::FETCH_ASSOC);
  
    if(count($results) != 0){
      session_start();
      $_SESSION["logged_user"] = $results;
      header("location: index.php");
    } else {
      $correct = false;
    }
  } else {
    session_start();
    session_destroy();
  }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSS -->
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" media="(max-width: 780px)" href="./css/responsive.css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="./libs/bootstrap/bootstrap.min.css">
    <title>• Login •</title>
  </head>
  <body style="margin: 0 !important">
    <section class="login-container">
      <div class="login-banner-container"></div>
      <div class="account-form-wrapper">
        <form id="formLogin" action="./login.php" class="account-form" method="post">
          <h1>Login</h1>
          <br>
          <div class="input-group">
            <input type="email" class="form-control" placeholder="Email" name="email">
            <input type="password" class="form-control" placeholder="Senha" name="password">
          </div>
          <br>
          <button class="btn btn-block btn-login">Login</button>
        </form>
        <br>
        <?php 
        
          if(isset($correct)){
            if($correct == false){
              echo "<span style='color: red'>Email ou Senha incorretos</span>";
            }
          }
        
        ?>
        <br>
        <br>
        <a href="./cadastro.php">Cadastre-se</a>
      </div>
    </section>

    <!-- jquery -->
    <script src="./libs/jquery/jquery-3.3.1.min.js"></script>
    
    <!-- Bootstrap -->
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
  
  </body>
</html>