<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSS -->
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" media="(max-width: 780px)" href="./css/responsive.css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="./libs/bootstrap/bootstrap.min.css">

    <title>• Cadastro •</title>
  </head>
  <body>
    <section class="cadastro-container">
      <div class="account-form-wrapper cadastro-form">
        <form class="account-form" id="formCadastro">
          <h1>Cadastro</h1>
          <br>
          <div class="input-group">
           <input type="text" class="form-control" placeholder="Nome" name="username">
          </div>
          <div class="input-group">
            <input type="tel" class="form-control" placeholder="Telefone" name="usertel" id="tel">
            <input type="text" class="form-control" placeholder="Endereço" name="useradress">
          </div>
          <div class="input-group">
              <input type="text" class="form-control" placeholder="CPF/CNPJ" name="usercnpj" id="cnpj">
          </div>
          <div class="input-group">
            <input type="email" class="form-control" placeholder="Email" name="usermail">
            <input type="password" class="form-control" placeholder="Senha" name="userpassword">
          </div>
          <br>
          <span class="text-pergunta">Pergunta</span>
          <br>
          <div class="pergunta-container">
            <div>
              <input id="res1" type="radio" class="" name="respostaPergunta" value="S" checked>
              <span>Resposta 1</span>
            </div>
            <div>
              <input id="res2" type="radio" name="respostaPergunta" value="O">
              <span for="res2">Resposta 2</span>
            </div>
          </div>
          <br>
          <button class="btn btn-login">Cadastrar</button>
        </form>
        <br>
        <a href="./login.php">Já tem uma conta?</a>
      </div>
    </section>

    <!-- jquery -->
    <script src="./libs/jquery/jquery-3.3.1.min.js"></script>
    
    <!-- jquery mask -->
    <script src="./libs/jquery-mask/jquery.mask.min.js"></script>

    <!-- Sweet Alerts -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

     <script>
    
      $(cnpj).mask('00.000.000/0000-00');
      $(tel).mask('(00)00000-0000');

      $(formCadastro).submit(function(e){
        e.preventDefault()
        if(this.username.value.length < 5 ||
           this.usertel.value.lenght < 5 ||
           this.useradress.value.lenght < 5 ||
           this.usercnpj.value.lenght < 11 ||
           this.usermail.value.lenght < 5 ||
           this.usermail.value.indexOf('@') == -1 ||
           this.usermail.value.indexOf('.') == -1 ||
           this.userpassword.value.lenght < 5){
            swal({
              title: "Erro ao Cadastrar",
              text: "Preencha todos os campos Corretamente",
              icon: "error",
            });
        } else {
          $(".btn-login").css({
            pointerEvents: 'none',
            opacity: '0.5'
          })
          chamaApi(this);
        }
      })

      function chamaApi(context){
        $.ajax({
          url: './includes/cadastra_usuario.php',
          type: 'post',
          data: $(context).serialize(),
          success: function(r){
            if(r == 1){
              swal({
                title: "Sucesso",
                text: 'Você foi Cadastrado!',
                icon: 'success'
              }).then(function(){
                location.href = "login.php"
              })
            } else if (r == 200) {
              swal({
                title: "Erro",
                text: 'Esse usuário já está cadastrado',
                icon: 'error'
              }).then(function(){
                permiteBotao();
              })
            } else if(r == 404) {
              swal({
                title: "Erro",
                text: 'Você não tem permissão, procure o responsável!',
                icon: 'error'
              }).then(function(){
                permiteBotao();
              })
            } else {
              swal({
                title: "Erro",
                text: 'Erro Interno',
                icon: 'error'
              })
            }
            console.log(r)
          }
        })
      }

      function permiteBotao(){
        $(".btn-login").css({
          pointerEvents: 'initial',
          opacity: '1'
        })
      }

    </script>

    <!-- Bootstrap -->
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
  </body>
</html>