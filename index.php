<?php 

  session_start();
  if(!isset($_SESSION["logged_user"])){
    header("location: login.php");
  } else {
    $userinfo = $_SESSION["logged_user"][0];
    echo "<script>var cnpjUsuario = " . $userinfo['cnpj'] . ";</script>";
  }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSS -->
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" media="(max-width: 780px)" href="./css/responsive.css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="./libs/bootstrap/bootstrap.min.css">
    <title>Dashboard •</title>
  </head>
  <body>
    <header class="main-header">
      <nav class="main-navbar">
        <span>
          <?php 
            echo $userinfo['nome'];
          ?>
        </span>
        <a href="./login.php">
          <button class="btn btn-loggout">Loggout</button>
        </a>
        <span>
          <?php
            
            include './includes/conn.php';
            $cupon = $conn->query("SELECT count(numerosorte) as quantidade FROM `numerosorte` WHERE cnpj='". $userinfo['cnpj'] ."' ORDER BY periodo DESC");
            $result = $cupon->fetch(PDO::FETCH_ASSOC);
            echo $result['quantidade'] . " Cupons";

          ?>
        </span>
      </nav>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="https://data.whicdn.com/images/310499483/original.jpg?t=1523379426" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="https://data.whicdn.com/images/310499483/original.jpg?t=1523379426" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="https://data.whicdn.com/images/310499483/original.jpg?t=1523379426" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>
    <br>
    <main>
      <section class="data-sorteios">
        <h3>Data dos Sorteios</h3>
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Data</th>
              <th>Número</th>
              <th>Prêmio</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>21/08/2018</td>
              <td>01</td>
              <td>Prêmio</td>
            </tr>
            <tr>
              <td>22/10/2018</td>
              <td>01</td>
              <td>Prêmio</td>
            </tr>
            <tr>
              <td>03/01/2019</td>
              <td>01</td>
              <td>Prêmio</td>
            </tr>
            <tr>
              <td>15/03/2019</td>
              <td>01</td>
              <td>Prêmio</td>
            </tr>
            <tr>
              <td>21/05/2019</td>
              <td>01</td>
              <td>Prêmio</td>
            </tr>
            <tr>
              <td>21/06/2019</td>
              <td>01</td>
              <td>Prêmio</td>
            </tr>
          </tbody>
        </table>
      </section>
      <br>
      <section class="numeros-sorte">
        <h3>Números da Sorte</h3>
        <div class="container-numeros" id="cardsContainer"></div>
      </section>
      <div id="numbersModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="numberModalTitle">Números da Sorte</h3>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Número do Cupom</th>
                  </tr>
                  <tbody id="listaCupons"></tbody>
                </thead>
              </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-login" data-dismiss="modal">Retornar</button>
            </div>
          </div>
        </div>
      </div>
    </main>
    <footer class="main-footer">


    </footer>

    <!-- jquery -->
    <script src="./libs/jquery/jquery-3.3.1.min.js"></script>
    
    <!-- Moment JS -->
    <script src="./libs/moment/moment.js"></script>

    <!-- API pra pegar os Cupons -->
    <script src="./js/cupons.js"></script>

    <!-- Bootstrap -->
    <script src="./libs/bootstrap/bootstrap.min.js"></script>
  
  </body>
</html>