$(document).ready(function () {
    moment.locale('pt-br');

    var fullData;

    // Obs: cpnjUsuario foi setado lá encima no php
    $.ajax({
        url: "./includes/getNumeros.php",
        type: "POST",
        format: "JSON",
        data: "cnpj=" + cnpjUsuario,
        success: function (data) {
            if (typeof data === 'object') {

                var periodos = {};

                data.forEach(function (numero) {
                    var periodo = new Date(numero.periodo + "T00:00:00")

                    if (!periodos[periodo.getFullYear()]) {
                        periodos[periodo.getFullYear()] = []
                    }

                    if (!periodos[periodo.getFullYear()][periodo.getMonth() + 1]) {
                        periodos[periodo.getFullYear()][periodo.getMonth() + 1] = []
                    }

                    periodos[periodo.getFullYear()][periodo.getMonth() + 1].push(numero)

                })

                fullData = periodos;

                for (ano in periodos) {
                    for (mes in periodos[ano]) {
                        var dadosMes = periodos[ano][mes];
                        if (dadosMes.length > 0) {
                            var strEl = "";
                            strEl += "<div class='numero-card' data-mes='" + mes + "' data-ano='" + ano + "'>";
                                strEl += "<div class='card-top'>" + dadosMes.length + "</div>";
                                strEl += "<div class='card-bottom'>" + moment(mes, 'M').format('MMMM').toUpperCase() + "/" + ano + "</div>";
                            strEl += "</div>"
                            $(cardsContainer).append(strEl);
                        }
                    }
                }
            }
            $(".numero-card").click(function () {
                $(numbersModal).modal();
                var ano = $(this).attr('data-ano'),
                    mes = $(this).attr('data-mes');
                $(listaCupons).empty();
                fullData[ano][mes].forEach(function (numero) {
                    $(listaCupons).append("<tr><td>" + numero.numerosorte + "</td></tr>");
                })
            })
        }
    })
})