<?php

  include './conn.php';

  // Variáveis
  $nome = $_POST["username"];
  $tel = $_POST["usertel"];
  $endereco = $_POST["useradress"];
  $cnpj = getCNPJ();
  $email = $_POST["usermail"];
  $senha = md5($_POST["userpassword"]);
  $pergunta = $_POST["respostaPergunta"];
  
  function getCNPJ(){
    $str = $_POST["usercnpj"];
    $str = str_replace(".", "", $str);
    $str = str_replace(",", "", $str);
    $str = str_replace("-", "", $str);
    $str = str_replace("/", "", $str);
    return $str;
  }
  
  function tem_permissao(){
    global $conn, $cnpj;
    $pegaUsers = $conn->query("SELECT count(cnpj) as quant FROM `elegiveis` WHERE cnpj='$cnpj'");
    $results = $pegaUsers->fetch(PDO::FETCH_ASSOC);
    if($results['quant'] > 0){
      return true;
    } else {
      return false;
    }
  }

  function ja_cadastrado(){
    global $conn, $cnpj;
    $pegaUsers = $conn->query("SELECT count(cnpj) as quant FROM `usuarios` WHERE cnpj='$cnpj'");
    $results = $pegaUsers->fetch(PDO::FETCH_ASSOC);
    if($results['quant'] > 0){
      return true;
    } else {
      return false;
    }
  }

  if(tem_permissao() == true){
    if(ja_cadastrado() == false){
      try {
        $query = "INSERT INTO usuarios
                  (nome, cnpj, telefone, endereco, email, senha, pergunta)
                  VALUES
                  (:nome, :cnpj, :tel, :endereco, :email, :senha, :pergunta)";
        $sql = $conn->prepare($query);
        $sql->execute(array(
          ':nome' => $nome,
          ':cnpj' => $cnpj,
          ':tel' => $tel,
          ':endereco' => $endereco,
          ':email' => $email,
          ':senha' => $senha,
          ':pergunta' => $pergunta
        ));
        echo true;
      }
      catch(PDOException $e)
      {
        echo $e->getMessage();
      }
    } else {
      echo 200;
    }
  } else {
    echo 404;
  }

?>