<?php

  header("Content-Type: application/json; charset=UTF-8");
  header("Access-Control-Allow-Origin: *");

  include './conn.php';

  $cnpj = $_POST["cnpj"];

  try {
    $selectNumbers = $conn->query("SELECT * FROM numerosorte ORDER BY periodo DESC");
    $numeros = $selectNumbers->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($numeros);
  } catch(PDOException $e) {
    echo $e->getMessage();
  }

?>